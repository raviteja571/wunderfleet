<?php
// bootstrap.php

//composer autoload to use namaspaces
require_once "vendor/autoload.php";
require_once "config.php";
//database connection
use Wunder\Db;
$db =  new Db();

// obtaining the entity manager
$entityManager = $db->entityManager;
