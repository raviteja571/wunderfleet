<?php
namespace Wunder;

use Wunder\Model;
use Wunder\View;
use Wunder\Presenter;
/**
 * Framework Class
 */ 
class Framework
{
    /**
     * Initial Method for the application
     */ 
    public static function run() 
    {
        self::init();
        self::dispatch();
    }
    
    /**
     * Adds all required configurations and preparing
     *     Actions
     */ 
    private static function init() 
    {
        session_start();
        
        include_once "config.php";
       
        $uri = explode('/', $_SERVER['REQUEST_URI']);

        define("ACTION", (isset($uri[1])  && $uri[1] != '' ) ? $uri[1] : 'index');

    }
    
    /**
     * Prepare and instantiate required Objects
     */ 
    private static function dispatch() 
    {
        $model   = new Model();
        $view    = new View();
        
        $presenter = new Presenter($model, $view);
        $action = ACTION . 'Action';
        if (method_exists($presenter, $action)) {
            echo $presenter->$action();
        } else {
            trigger_error("Unable to load Action: $action", E_USER_WARNING);
            echo "<center><b>".$_SERVER['REQUEST_URI']." does not exist, please check. sorry</b></center>";
            exit;
        }    
    }
    
}
