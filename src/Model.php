<?php

namespace Wunder;
use Wunder\Entity\Users;
use Wunder\Db;
/**
 * Model Class
 */ 
class Model
{
    /**
     * @var data
     */
    protected $data;
    /**
     * @var entityManager
     */
    protected $entityManager;
    
    /**
     * constructer for setters
     */
    public function __construct() 
    {
        $this->setEntityManager();
    }    
    
    /**
     * This method sets teh user data and
     * dump the data to databse
     * using Doctrine entity Manaager
     * 
     * @param array $user
     * 
     * @return object $userEntity
     */
    public function setUserdata($user)
    {
        // obtaining the entity manager
        $entityManager = $this->entityManager;
        $userEntity = $this->getUserdata($user['email']);
        if(!$userEntity) {
            $userEntity = new Users();
        }                 
        
        switch($user['current']) {
        case 0:
            $_SESSION['email'] = $user['email'];
            $userEntity->setEmail($user['email']);
            $userEntity->setProcessStep($user['current']);
            break;
        case 1:
            $userEntity->setFirstname($user['firstname']);
            $userEntity->setLastname($user['lastname']);
            $userEntity->setPhonenumber($user['phonenumber']);
            $userEntity->setProcessStep($user['current']);
            break;
        case 2:
            $userEntity->setStreet($user['street']);
            $userEntity->setHouseno($user['houseno']);
            $userEntity->setZip($user['zip']);
            $userEntity->setCity($user['city']);
            $userEntity->setProcessStep($user['current']);
            break;
        case 3:
            $userEntity->setOwnername($user['accountowner']);
            $userEntity->setIban($user['iban']);
            $userEntity->setProcessStep($user['current']);
            break;
        }
        
        $user = $entityManager->getRepository('Wunder\Entity\Users')
            ->findOneBy(array('email' => $user['email']));

        if(!$user) {
            $entityManager->persist($userEntity);
            $entityManager->flush();
        } else {
            $entityManager->flush();
        }
        
        return $userEntity;
    }
    
    /**
     * This method retrieves the user data
     * by passing email as parameter
     * 
     * @param string email email
     * 
     * @return object $user user
     */
    public function getUserdata($email)
    {
        $user = $this->entityManager->getRepository('Wunder\Entity\Users')
            ->findOneBy(array('email' => $email));
        return $user;
    }

    /**
     * This Method prepares json data for Payment Processiing
     * and passes them for CURL action
     * 
     * @param array $data data
     * 
     * @return object $jsonObject jsonObject
     */
    public function doPayment($data)
    {
        $jsonResult = null;
        $this->data['customerId'] = $data['id'];
        $this->data['iban'] = $data['iban'];
        $this->data['owner'] = $data['owner'];
        
        $jsonResult = $this-> _doCurl($this->data);

        return $jsonResult;
    }
    /**
     * CURL operations
     * 
     * @param array $data data
     * 
     * @return object $serverOutput serverOutput
     */
    private function _doCurl($data) 
    {
        
        $payload = json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, PAYMENTURL);
        curl_setopt($ch, CURLOPT_POST, 1);
        
        // In real life you should use something like:
        curl_setopt(
            $ch, CURLOPT_POSTFIELDS, 
            $payload
        );
                  
        // Receive server response ...
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $serverOutput = json_decode(curl_exec($ch));
        
        curl_close($ch);
        
        return $serverOutput;
    }
    
    /**
     * This method is responsible for updating the payment information
     * after successful curl operation
     * 
     * @param object $data data
     * @param int    $id   id
     * 
     * @return boolean
     */
    public function updatePayment($data, $id)
    {
        $userEntity = $this->entityManager->find('Wunder\Entity\Users', $id);
        $userEntity->setPaymentdataid($data->paymentDataId);
        $this->entityManager->flush();
        
        return true;
    }
    
    /**
     * Setter for entity Manager
     */
    public function setEntityManager() 
    {
        $db =  new Db();
        
        // obtaining the entity manager
        $this->entityManager = $db->entityManager;
    }
    
    /**
     * Setter Process id
     */
    public function setProcess($email) 
    {
		$user = $this->entityManager->getRepository('Wunder\Entity\Users')
            ->findOneBy(array('email' => $email));
		$user->setProcessStep($user->getProcessStep()-1);
        $this->entityManager->flush();
    }
    
}
