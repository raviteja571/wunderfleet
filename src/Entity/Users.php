<?php
// src/Users.php
namespace Wunder\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class Users
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $email;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $firstname;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lastname;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $phonenumber;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $street;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $houseno;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $zip;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $city;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $ownername;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $iban;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $paymentdataid;
    
    /**
     * @ORM\Column(type="integer", length=10, nullable=true)
     */
    protected $process_step;
    /**
     * getId
     */ 
    public function getId()
    {
        return $this->id;
    }
    /**
     * getId
     */
    public function getFirstname()
    {
        return $this->firstname;
    }
    /**
     * set firstname
     * 
     * @param firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }
    /**
     * get email
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * set email
     * 
     * @param email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
    /**
     * get lastname
     */
    public function getLastname()
    {
        return $this->lastname;
    }
    /**
     * set lastname
     * 
     * @param lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }
    /**
     * get phonenumber
     */
    public function getPhonenumber()
    {
        return $this->phonenumber;
    }
    /**
     * set phonenumber
     * 
     * @param phonenumber
     */
    public function setPhonenumber($phonenumber)
    {
        $this->phonenumber = $phonenumber;
    }
    /**
     * get street
     */
    public function getStreet()
    {
        return $this->street;
    }
    /**
     * set street
     * 
     * @param street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }
    /**
     * get houseno
     */
    public function getHouseno()
    {
        return $this->houseno;
    }
    /**
     * set houseno
     * 
     * @param houseno
     */
    public function setHouseno($houseno)
    {
        $this->houseno = $houseno;
    }
    /**
     * get zip
     */
    public function getZip()
    {
        return $this->zip;
    }
    /**
     * set zip
     * 
     * @param zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }
    /**
     * get city
     */
    public function getCity()
    {
        return $this->city;
    }
    /**
     * set city
     * 
     * @param city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }
    /**
     * get Ownername
     */
    public function getOwnername()
    {
        return $this->ownername;
    }
    /**
     * set ownername
     * 
     * @param ownername
     */
    public function setOwnername($ownername)
    {
        $this->ownername = $ownername;
    }
    /**
     * get iban
     */
    public function getIban()
    {
        return $this->iban;
    }
    /**
     * set iban
     * 
     * @param iban
     */
    public function setIban($iban)
    {
        $this->iban = $iban;
    }
    /**
     * get paymentdataid
     */
    public function getPaymentdataid()
    {
        return $this->paymentdataid;
    }
    /**
     * set paymentdataid
     * 
     * @param paymentdataid
     */
    public function setPaymentdataid($paymentdataid)
    {
        $this->paymentdataid = $paymentdataid;
    }
    /**
     * get process_step
     */
    public function getProcessStep()
    {
        return $this->process_step;
    }
    /**
     * set process_step
     * 
     * @param process_step
     */
    public function setProcessStep($process_step)
    {
        $this->process_step = $process_step;
    }
    
    /**
     * @return array
     */
    public function getArray(): array
    {
        return [
            'id' => $this->getId(),
            'email' => $this->getEmail(),
            'firstname' => $this->getFirstname(),
            'lastname' => $this->getLastname(),
            'phonenumber' => $this->getPhonenumber(),
            'street' => $this->getStreet(),
            'houseno' => $this->getHouseno(),
            'zip' => $this->getZip(),
            'city' => $this->getCity(),
            'iban' => $this->getIban(),
            'ownername' => $this->getOwnername(),
            'paymentdataid' => $this->getPaymentdataid(),
            'current' => $this->getProcessStep(),
        ];
    }
}
