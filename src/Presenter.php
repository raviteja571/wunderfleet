<?php
/**
 * Main namespace
 */ 
namespace Wunder;

/**
 * Presenter Class
 */ 
class Presenter
{
 
    private $model;

    private $view;
    
	/**
     * Constructer
     */ 
    public function __construct(Model $model, View $view)
    {
        $this->model = $model;
        $this->view = $view; 
    }
	
	/**
     * default method 
     */ 
    public function indexAction()
    {
        $data = array();

        // Variables are set now, render the HTML
        // And returns as a string
        return $this->view->render('Views/register.phtml', $data);
    }
    
    /**
     * register action to final step 
     */ 
    public function registerAction()
    {
        $user = $this->model->setUserdata($_POST);

        $data['id'] = $user->getId();
        $data['iban'] = $user->getIban();
        $data['owner'] = $user->getOwnername();
        
        
        
        $result  = $this->model->doPayment($data);
        if (!isset($result->paymentDataId)) {
            return $this->view->render('Views/failure.phtml', array('message' => 'Something Went wrong!'));
        } else {
            $this->model->updatePayment($result, $data['id']);
            return $this->view->render('Views/success.phtml', array('message' => $result->paymentDataId    ));
        }
    }
    
    /**
     * Save data on every step
     */ 
    public function saveAction()
    {
        $user = $this->model->getUserdata($_POST['email']);
        if ($user && $_POST['current'] == 0) {
            $data = $user->getArray();
            if ($data['current'] != 3) {
                return $this->view->render('Views/register.phtml', $data);
            } else {
                return $this->view->render('Views/success.phtml', array('message' => $data['paymentdataid'], 'extramsg' =>    'You already did the Payment.'));
            }
        } else {
            $user = $this->model->setUserdata($_POST);
            $data = $user->getArray();
            if ($_POST['current'] == -1) {
                $data['current'] = $data['current']; 
                $user = $this->model->setProcess($data['email']);
            } else {
                $data['current'] = $data['current'] + 1; 
            }

            return $this->view->render('Views/register.phtml', $data);
        }
     
    }
}
