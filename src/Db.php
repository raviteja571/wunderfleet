<?php
namespace Wunder;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\DBALException;
/**
 * Database Connection
 */
class Db
{
    /**
     * entity Manager
     */
    public $entityManager;
    
    /**
     * Constructer
     */
    public function __construct() 
    {
        // Create a simple "default" Doctrine ORM configuration for Annotations
        $isDevMode = true;
        $config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/"), $isDevMode, null, null, false);

        // database configuration parameters
        $conn = array(
        'driver' => 'pdo_mysql',
        'host'     => DBHOST,
        'dbname'   => DBNAME,
        'user'     => DBUSER,
        'password' => PASSWORD
        );

        // obtaining the entity manager
        try {
            $this->entityManager = EntityManager::create($conn, $config);
        } catch(DBALException $e) {
            echo "General Error: The user could not be added.<br>".$e->getMessage();
        }
    }
}
