<?php
namespace Wunder;
/**
 * View Class
 */ 
class View
{
    /**
     * Renderer for the view from Presenter
     * 
     * @param filepath $path path
     * @vars  array $vars data
     * 
     * @return renderer
     */ 
    public function render($path, array $vars = array())
    {
        ob_start();
        extract($vars);
        include $path;
        return ob_get_clean();
    }
}
