<?php
// cli-config.php

//This file is responsible for creating the database tables from Entity files
require_once "bootstrap.php";

use Wunder\Framework;

Framework::run();

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);
