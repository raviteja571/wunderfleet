# Wunderfleet Test Task - README #

Compatible with PHP7.2 and Modified for clean code standards using PHPCS and PHPMD

### What is this repository for? ###

* Quick summary
	* Registration form
	* Database using Dcotrine
	* Payment Integration
* Version 
* [Test Task](https://bitbucket.org/raviteja571/wunderfleet.git)

### How do I get set up? ###

*  use ```composer install``` before start with the functionality
*  use ```composer dump-autoload``` to update autoload classes  
*  Create Database and add the configuration in config.php file
*  Update Database entities by running ```vendor/bin/doctrine orm:schema-tool:update --force --dump-sql```
*  Goto ROOT directory and use something like ```php -S 127.0.0.1:8080``` to run on browser

### Improvements Done
*  Used ```Doctrine``` for easily creating databases using ```EntityManager```
*  Composer usage to setup the project in any defined environment easily
*  "Functionality" - Added ```email``` field  to make it as central element to retrive the saved form
*  Followd MVP Pattern for Url hiding
*  Added ```Previous``` button to edit the previous data
  

### Performance Optimizations need to be done ###
*  Memcaching System like Redis for temperarly saving user data
*  Ajax form loading using Jquery
