<?php
//This config file is to define all the common variables and supply them
//throught the project
define('DS', DIRECTORY_SEPARATOR);
        
define('ROOT', getcwd().DS);

define("PUBLIC_PATH", ROOT . "out" . DS);

//DATABASE CONFIGURATION
define('DBHOST', 'localhost');
define('DBNAME', 'wunderfleet');
define('DBUSER', 'developer');
define('PASSWORD', 'Me@570571');

//Payment Gateway
define("PAYMENTURL", 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data');

